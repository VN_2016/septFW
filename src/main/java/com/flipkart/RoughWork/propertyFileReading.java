package com.flipkart.RoughWork;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class propertyFileReading {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		String ProjectPath= System.getProperty("user.dir");
		System.out.println(ProjectPath);
		
		Properties config= new Properties();
		FileInputStream ip= new FileInputStream(new File(ProjectPath+"\\src\\main\\resources\\CONFIG.properties"));
		
		config.load(ip);
		
		
		System.out.println(config.getProperty("TestURL"));
		
	}

}
