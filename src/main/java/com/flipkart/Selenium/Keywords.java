package com.flipkart.Selenium;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.android.library.ChromeClientWrapper;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.flipkart.Base.TestBase;

public class Keywords extends TestBase {

	//openbroswer
	
	public static void openBrowser(){
		String browser=CONFIG.getProperty("browserType");
		String ffProfile=CONFIG.getProperty("firefoxProfile");
		int implicitwait=Integer.parseInt(CONFIG.getProperty("implicitwait"));
		
		
		DesiredCapabilities caps= new DesiredCapabilities();
		caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		
		if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.drive", PROJECT_PATH+"\\chromedriver.exe");
			driver= new ChromeDriver(caps);
		}else if(browser.equalsIgnoreCase("FF")){
			ProfilesIni profiles= new ProfilesIni();
			FirefoxProfile fp=profiles.getProfile(ffProfile);
			
			driver= new FirefoxDriver(fp);
		}else if(browser.equalsIgnoreCase("IE")){			
			System.setProperty("webdriver.ie.drive", PROJECT_PATH+"\\IEDriverServer.exe");
			driver= new InternetExplorerDriver(caps);
		}else{
			System.setProperty("webdriver.chrome.drive", PROJECT_PATH+"\\chromedriver.exe");
			driver= new ChromeDriver(caps);		
		}
		
		driver.manage().timeouts().implicitlyWait(implicitwait, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
	}
	
	
	
	public static void navigateToURL(String URL){
		driver.navigate().to(URL);
	}
	
	
	public static void navigateBack(){
		driver.navigate().back();
	}
	
	
	public static void navigateForward(){
		driver.navigate().forward();
	}
	
	public static void refreshpage(){
		driver.navigate().refresh();
	}
	
	public static void click(WebElement elem){
		elem.click();
	}
	
	public static void type(WebElement elem, String data){
		elem.sendKeys(data);
	}
	
	
	
	public static String getElementText(WebElement elem){
		return elem.getText();
	}
	
	
	
	public static boolean verifyElementText(WebElement elem, String expectedText){
	
		String actualText= getElementText(elem);
		if(actualText.equalsIgnoreCase(expectedText)){
			return true;
		}else{
			return false;
		}
		
	}
	
	
	
	
	
	
	
	
}
