package com.flipkart.Utilities;

public class DataUtil {
	
	
	
	public static Object[][] getTestdata(String filePath, String SheetName){
		
		Xls_Reader xls= new Xls_Reader(filePath);
		int rows=xls.getRowCount(SheetName);
		int cols=xls.getColumnCount(SheetName);
		
		Object[][] data= new Object[rows-1][cols];//3,2
		
		for(int i=0; i<data.length;i++){
			for(int j=0;j<data[0].length;j++){
				data[i][j]=xls.getCellData(SheetName, j, i+2);
				
			}
		}
		
		return data;
		
	}

}
